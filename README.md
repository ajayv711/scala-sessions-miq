### System Requirements
+ Java 1.8
+ sbt [1]
+ Scala [2]
+ Intellij IDEA (recommended)

### Running the examples
Once you have sbt installed, you can run the examples in the code by executing `sbt run` from your project root directory and picking the relevant `Main` class [3]

**Basic SBT commands**
```
sbt clean
sbt clean compile
sbt clean package
sbt clean assembly
sbt test
sbt testOnly [test-class]
```

### Footnotes
[1]: https://www.scala-sbt.org/1.0/docs/Installing-sbt-on-Linux.html
[2]: https://www.scala-lang.org/download/
[3]: "Scala allows you to maintain multiple `Main` classes in one project"

### Resources
Compendium of useful training resources for Scala

+ [**[Recommended reading]** Scala school, Twitter](https://twitter.github.io/scala_school/)
+ [**[Optional reading]** Introduction to Lambda calculus](https://brilliant.org/wiki/lambda-calculus/)
+ [**[Advanced]** Effective Scala](http://twitter.github.io/effectivescala/)
+ [**[Recommended reading]** Functional Programming in Scala](https://www.manning.com/books/functional-programming-in-scala)
+ [**[Recommended reading]** Neophyte's guide to Scala](http://danielwestheide.com/scala/neophytes.html)
+ [**[Practical scala]** Scala cookbook](http://www.bigdataanalyst.in/wp-content/uploads/2015/07/Scala-Cookbook.pdf)

